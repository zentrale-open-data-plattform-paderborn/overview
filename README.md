# Projektdokumentation: Zentrale Open Data Plattform der Stadt Paderborn

**Inhaltsverzeichnis**

[[_TOC_]]

## Allgemeines
Herzlich Willkommen auf der Startseite der technischen Projektdokument des Förderprojektes Zentrale Open Data Plattform der Stadt Paderborn. Weitere generelle Informationen zum Projekt finden Sie auf der Seite der [Digitalen Heimat Paderborn](https://digitale-heimat-pb.de/projekte/zentrale-open-data-plattform/).

Diese Seite dient Ihnen als erster zentraler Einstiegspunkt für die technische Projektdokumentation. Auf den folgenden Seiten finden Sie die aktuellen Arbeitsstände der Plattformkomponenten und den entsprechenden, unter einer jeweiligen Open Source Lizenz veröffentlichten, Quellcode der Software.

Bitte beachten Sie, dass es sich bei den Inhalten der ersten Veröffentlichung im Juni 2021 um einen frühen prototypischen Zwischenstand der Datenplattform handelt. 

Die zweite Veröffentlichung gegen Ende Juli 2022 spiegelt den Stand der Entwicklung zu Projektende wieder.   

Mithilfe der folgenden Links gelangen Sie direkt zu jeweiligen Repositories der einzelnen Komponenten:

## Inhalte der ersten Veröffentlichung (Stand Juni 2021)

### Gruppe *fiware*

- [Installationsanleitung FIWARE Datenplattform](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation)

- [Übergreifende Dokumentation FIWARE Datenplattform](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/documentation)

- [Orion Context Broker](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/orion)

- [APInf Umbrella](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/umbrella)

- [APInf Plattform](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/platform)

- [Tenant Manager](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/tenantmanager)

- [Perseo](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/perseo)

- [Node-RED](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/node-red-on-k8s)

- [Knowage](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/knowage)

- [Keyrock](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/keyrock)

- [Grafana](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/grafana)

- [Cosmos](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/cosmos)

- [Kurento](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/kurento)

- [Mongo DB](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/mongo)

- [QuantumLeap & Crate DB](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/quantumleap)

Hier finden Sie ein kurzes Video zur aktuellen FIWARE basierten Datenplattform unseres Dienstleisters auf Englisch: https://www.youtube.com/watch?v=R_JreCtls9A


### Gruppe *Anliegenmanagement*

- [Sag's Paderborn](https://gitlab.com/zentrale-open-data-plattform-paderborn/anliegenmanagement/sags-paderborn)


## Inhalte der zweiten Veröffentlichung (Projektende, Stand Juli 2022)

Das folgende Bild zeigt grob die Komponenten der Plattform, wie Sie momentan in Paderborn umgesetzt ist. Die ursprüngliche Planung sah vor, alle aufgeführten Komponenten, insbesondere die Use-Case Komponenten, mit in das Repository zu übernehmen.  

![img](img/zodp_pb.jpg)

Leider konnten die Arbeiten nicht an allen Use-Case Komponenten bis zum Zeitpunkt der Veröffentlichung abgeschlossen werden. Im Wesentlichen ging es dabei um die Entfernung Paderborn-spezifischer Inhalte aus diesen Komponenten und nachfolgend die Verifikation des Ausrollprozesses, die Dokumentation und den Integrationstest.

### Gruppe *urban-dataspace*

- [Core Plattform](https://gitlab.com/zentrale-open-data-plattform-paderborn/urban-dataspace/core-platform)

Die in dem folgenden Bild blau und dunkelblau dargestellten Komponenten sind Bestandteil der Veröffentlichung in diesem Projekt des Paderborner Repositorys.

![img](img/zodp_pub.jpg)

Für die Basis-Komponenten der Paderborner Use-Cases (CKAN, Masterportal, Drupal und einige einfache Node-RED Flows zum Zugriff auf externe Datenquellen) verweisen wir aus den oben angeführten Gründen daher auf den aktuellen Entwicklungsstand in dem Repository mit der Adresse

https://gitlab.com/urban-dataspace-platform,

welches von einem der Dienstleister unseres Projektes angelegt wurde und gepflegt wird.

## Austausch zum Projekt
Obgleich die hier veröffentlichten Inhalte als Basis und Einstieg in die Plattformtechnologie allgemein dienen können, sind wesentliche Punkte, insbesondere im Hinblick auf den notwendigen personellen Aufwand und die laufenden Kosten zum Betrieb, noch nicht hinreichend addressiert.
Mit den im Projekt gesammelten Erfahrungen arbeitet das Team der Stadt Paderborn intensiv an der Weiterentwicklung der Plattform, wobei mittelfristig folgende Punkte in den Fokus der Aktivitäten rücken:

- Vermeidung von Doppel bzw. Mehrfach-Datenhaltung
- Konsolidierung der Datenbanktypen über Komponentengrenzen hinweg
- (Teil-)Automatisierung des IoT-Device On-boardings
- Einführung eines umfassenden Datenmanagements
- Weitere Vereinfachung des Ausrollverfahrens (Everything as Code)
- Vorbereitung für den Betrieb auf Green-IT Technologie

Binnen Jahresfrist plant die Stadt Paderborn die Veröffentlichung einer komplett überarbeiteten Plattformarchitektur. Der Open-Source Ansatz wird dabei strikt weiterverfolgt.

Wenn Sie dazu direkt mit dem Team der Stadt Paderborn Kontakt aufnehmen möchten, verwenden Sie bitte folgende E-Mail Adresse: 

open-data@paderborn.de

## Förderhinweis

Die Ergebnisse dieses Projektes sind im Rahmen einer Förderung der digitalen Modellregion Ostwestfalen-Lippe des Landes Nordrhein-Westfalen entstanden.

![img](img/logoleiste.JPG)


## Lizenz
Copyright © 2021 Stadt Paderborn. This work is licensed under a CC BY SA 4.0 [license](https://creativecommons.org/licenses/by-sa/4.0/). 
